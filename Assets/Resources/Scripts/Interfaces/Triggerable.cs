﻿using UnityEngine;
using System.Collections;

// For use with the player "use" functionality and doors.

public interface Triggerable
{
	void Trigger();
}
