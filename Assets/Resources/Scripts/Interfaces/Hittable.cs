﻿using UnityEngine;
using System.Collections;

public interface Hittable 
{
	// Pass the direction information of the raycast.
	void OnHit(Vector3 hit, int value);
}

