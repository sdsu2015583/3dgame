﻿using UnityEngine;
using System.Collections.Generic;
using System;

// This is how ammo is recorded.
// Ammo may have any ammoType string, and may be used by any weapon with a matching specification.
public class PlayerAmmoTracker : MonoBehaviour
{
	[HideInInspector] public Dictionary<string, AmmoType> ammoStorage = new Dictionary<string, AmmoType>();

	[System.Serializable]
	public class AmmoType : IComparable<AmmoType>
	{
		// Type of ammo, for use in our dictionary. These can be specified with strings.
		public string ammoType;
		public int count;
		public AmmoType(string type)
		{
			this.ammoType = type;
			this.count = 0;
		}
		public AmmoType(string type, int _count)
		{
			this.ammoType = type;
			this.count = _count;
		}
		public int CompareTo(AmmoType a)
		{
			return String.Compare(this.ammoType, a.ammoType);
		}
	}

	public void AddAmmo(ItemAmmo i)
	{
		AmmoType tmp;
		if(ammoStorage.TryGetValue(i.ammoType, out tmp))
		{
			ammoStorage[i.ammoType].count += i.value;
		}
		else
		{
			ammoStorage.Add(i.ammoType,i.ammoTypeInstance);
			ammoStorage[i.ammoType].count = i.value;
		}
	}
}
