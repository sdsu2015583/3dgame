﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

/*	This is a class of classes.
 *  Each of these tags is able to represent the important components of their
 *  respective GameObjects in serializable terms.
 * 
 *  Every tag entry has a specific way of reinitializing/instantiatingn itself in their respective Create functions.
 * 
 *  This must be assigned to the GameManager object to function properly.
 *  All item/weapon/enemy/etc prefabs must be assigned to their respective arrays
 * 	in the inspector for this to function properly.
 */

public class SaveTag : MonoBehaviour
{
	public GameObject[] weapons;
	public GameObject[] items;
	public GameObject[] enemies;
	public GameObject[] worldObjects;
	public GameObject[] enemySpawners;

	void Awake()
	{
		gameObject.name = "GameManager";
	}

	public interface Tag	// So we can use generic magic in SaveController
	{
		void Create();
	}

	[System.Serializable]
	public class WeaponTag : Tag
	{
		public float [] vect = new float[3];
		public float [] quat = new float[4];
		
		public string weaponName;

		public bool dropped;
		public bool firing;
		public bool pickedUp;
		
		public WeaponTag(Weapon w)
		{
			Transform t = w.gameObject.transform;
			vect[0] = t.position.x; vect[1] = t.position.y; vect[2] = t.position.z;
			quat[0] = t.rotation.x; quat[1] = t.rotation.y; quat[2] = t.rotation.z; quat[3] = t.rotation.w;
			weaponName = w.weaponName;
			pickedUp = w.pickedUp;
			dropped = w.dropped;
			firing = w.firing;
		}
		public void Create()
		{
			GameObject [] weapons = GameObject.Find("GameManager").GetComponent<SaveTag>().weapons;
			for(int i = 0; i < weapons.Length; i++)
			{
				if(weaponName.Equals(weapons[i].GetComponent<Weapon>().weaponName))
				{
					GameObject tmp =GameObject.Instantiate(weapons[i],
					            new Vector3(vect[0],vect[1],vect[2]),
					            new Quaternion(quat[0],quat[1],quat[2],quat[3]))
								as GameObject;

					tmp.GetComponent<Weapon>().player = GameObject.Find("Player");
					tmp.GetComponent<Weapon>().pickedUp = pickedUp;
					tmp.GetComponent<Weapon>().dropped = dropped;
					tmp.GetComponent<Weapon>().firing = firing;

					if(pickedUp)
					{
						tmp.GetComponent<Weapon>().BindToPlayer(tmp.GetComponent<Weapon>().player);
					}
					break;
				}
			}
		}
	}

	[System.Serializable]	
	public class ItemTag : Tag
	{
		public float [] vect = new float[3];
		public float [] quat = new float[4];

		public bool isAmmo;

		public string itemType;
		public string ammoType;

		public int itemValue;
		public ItemTag(Item i)
		{
			Transform t = i.gameObject.transform;
			vect[0] = t.position.x; vect[1] = t.position.y; vect[2] = t.position.z;
			quat[0] = t.rotation.x; quat[1] = t.rotation.y; quat[2] = t.rotation.z; quat[3] = t.rotation.w;
			itemType = i.itemType;
			itemValue = i.value;

			ItemAmmo a = i.gameObject.GetComponent<ItemAmmo>();
			if(a != null)
			{
				isAmmo = true;
				ammoType = a.ammoType;
			}
		}
		public void Create()
		{
			GameObject [] items = GameObject.Find("GameManager").GetComponent<SaveTag>().items;
			for(int i = 0; i < items.Length; i++)
			{
				if(itemType.Equals(items[i].GetComponent<Item>().itemType))
				{
					if(isAmmo)
						if(!ammoType.Equals(items[i].GetComponent<ItemAmmo>().ammoType))
							continue;
					
					GameObject tmp = Instantiate(items[i],
					                             new Vector3(vect[0],vect[1],vect[2]),
					                             new Quaternion(quat[0],quat[1],quat[2],quat[3]))
						as GameObject;
					tmp.GetComponent<Item>().value = itemValue;
					break;
				}
			}
		}
	}

	[System.Serializable]
	public class EnemyTag : Tag
	{
		public float [] vect = new float[3];
		public float [] quat = new float[4];

		public string enemyType;
		public float health;
		public float armor;
		public bool constantHealth;
		public bool constantArmor;
		public EnemyTag(Enemy e)
		{
			Transform t = e.gameObject.transform;
			vect[0] = t.position.x; vect[1] = t.position.y; vect[2] = t.position.z;
			quat[0] = t.rotation.x; quat[1] = t.rotation.y; quat[2] = t.rotation.z; quat[3] = t.rotation.w;

			enemyType = e.enemyType;
			health = e.health;
			armor = e.armor;
			constantHealth = e.constantHealth;
			constantArmor = e.constantArmor;
		}
		public void Create()
		{
			GameObject [] enemies = GameObject.Find("GameManager").GetComponent<SaveTag>().enemies;
			for(int i = 0; i < enemies.Length; i++)
			{
				if(enemyType.Equals(enemies[i].GetComponent<Enemy>().enemyType))
				{
					GameObject tmp = Instantiate(enemies[i],
					                             new Vector3(vect[0],vect[1],vect[2]),
					                             new Quaternion(quat[0],quat[1],quat[2],quat[3])) 
						as GameObject;
					tmp.GetComponent<Enemy>().health = health;
					tmp.GetComponent<Enemy>().armor = armor;
					tmp.GetComponent<Enemy>().constantHealth = constantHealth;
					tmp.GetComponent<Enemy>().constantArmor = constantArmor;
					break;
				}
			}
		}
	}

	[System.Serializable]
	public class PlayerTag : Tag
	{
		public float [] vect = new float[3];
		public float [] quat = new float[4];

		public float health;
		public float armor;
		public bool constantHealth;
		public bool constantArmor;
		public bool hasObjective;
		public bool isArmed;
		public bool isAlive;

		public Dictionary<string, PlayerAmmoTracker.AmmoType> ammoStorage;

		public string currentLevel;

		public PlayerTag(Player p)
		{
			Transform t = p.gameObject.transform;
			vect[0] = t.position.x; vect[1] = t.position.y; vect[2] = t.position.z;
			quat[0] = t.rotation.x; quat[1] = t.rotation.y; quat[2] = t.rotation.z; quat[3] = t.rotation.w;

			health = p.health;
			armor = p.armor;
			constantHealth = p.constantHealth;
			constantArmor = p.constantArmor;
			hasObjective = p.hasObjective;
		}
		public void Create()
		{
			GameObject player = GameObject.Find ("Player");

			if(GameObject.Find("SceneMarker").GetComponent<Scene>().fromLevel == false)
			{
				player.transform.position = new Vector3(vect[0],vect[1],vect[2]);
				player.transform.rotation = new Quaternion(quat[0],quat[1],quat[2],quat[3]);
			}

			player.GetComponent<Player>().health = health;
			player.GetComponent<Player>().armor = armor;
			player.GetComponent<Player>().constantHealth = constantHealth;
			player.GetComponent<Player>().constantArmor = constantArmor;
			player.GetComponent<Player>().hasObjective = hasObjective;

			player.GetComponentInChildren<FirstPersonController>().m_MouseLook.Init(player.transform,player.GetComponentInChildren<Camera>().transform);
			player.GetComponent<Player>().isArmed = false; // So we can pick weapons up.
			player.GetComponent<Player>().isAlive = true; // You can't save when you're dead, so assume the player isnt dead.
		}
	}

	[System.Serializable]
	public class SceneTag : Tag
	{
		public string name;
		public string level;
		public bool fromLevel;

		public SceneTag(Scene s)
		{
			name = s.name;
			level = s.level;
			fromLevel = s.fromLevel;
		}
		public void Create()
		{
			GameObject marker = new GameObject("TEMP");
			marker.AddComponent<Scene>();
			marker.name = name;
			marker.GetComponent<Scene>().fromLevel = fromLevel;
		}
	}

	[System.Serializable]
	public class WObjectTag : Tag
	{
		public float [] vect = new float[3];
		public float [] scal = new float[3];
		public float [] quat = new float[4];

		public string worldObjectType;
		public bool active;

		public WObjectTag(WorldObject w)
		{
			active = w.active;
			worldObjectType = w.worldObjectType;
			Transform t = w.gameObject.transform;
			vect[0] = t.position.x; vect[1] = t.position.y; vect[2] = t.position.z;
			scal[0] = t.localScale.x; scal[1] = t.localScale.y; scal[2] = t.localScale.z;
			quat[0] = t.rotation.x; quat[1] = t.rotation.y; quat[2] = t.rotation.z; quat[3] = t.rotation.w;
		}
		public void Create()
		{
			GameObject [] worldObjects = GameObject.Find("GameManager").GetComponent<SaveTag>().worldObjects;
			for(int i = 0; i < worldObjects.Length; i++)
			{
				if(worldObjectType.Equals(worldObjects[i].GetComponent<WorldObject>().worldObjectType))
				{
					GameObject tmp = Instantiate(worldObjects[i],
					                             new Vector3(vect[0],vect[1],vect[2]),
					                             new Quaternion(quat[0],quat[1],quat[2],quat[3]))
						as GameObject;

					tmp.transform.localScale = new Vector3(scal[0],scal[1],scal[2]);
					tmp.GetComponent<WorldObject>().worldObjectType = worldObjectType;
					print(active + " " + tmp.GetComponent<WorldObject>().active);
					if(!active)
						tmp.gameObject.GetComponent<WorldObject>().Disable();
					break;
				}
			}
		}
	}
	
	[System.Serializable]
	public class ESpawnerTag : Tag
	{
		public float [] vect;
		public float [] quat;
		
		public float [] sp1;
		public float [] sp2;
		public float [] sp3;
		public float [] sp4;
		
		public string enemyToSpawn;

		public bool active;
		public bool continuous;
		public bool randomPosition;
		public int spawnCount;
		public float spawnDelay;
		
		public ESpawnerTag(EnemySpawner s)
		{
			enemyToSpawn = s.enemyToSpawn;
			active = s.active;
			continuous = s.continuous;
			randomPosition = s.randomPosition;
			spawnCount = s.spawnCount;
			spawnDelay = s.spawnDelay;
			
			Transform t = s.gameObject.transform;
			
			vect = Library.Vector3ToFloatArray(t.position);
			quat = Library.QuatnerionToFloatArray(t.rotation);
			
			sp1 = Library.Vector3ToFloatArray(s.spawnPoints[0].transform.position);
			sp2 = Library.Vector3ToFloatArray(s.spawnPoints[1].transform.position);
			sp3 = Library.Vector3ToFloatArray(s.spawnPoints[2].transform.position);
			sp4 = Library.Vector3ToFloatArray(s.spawnPoints[3].transform.position);
			
		}
		public void Create()
		{
			GameObject [] enemySpawners = GameObject.Find("GameManager").GetComponent<SaveTag>().enemySpawners;
			for(int i = 0; i < enemySpawners.Length; i++)
			{
				Vector3 v = Library.FloatArrayToVector3(vect);
				Quaternion q = Library.FloatArrayToQuatnerion(quat);

				GameObject tmp = Instantiate(enemySpawners[i],v,q) as GameObject;

				tmp.GetComponent<EnemySpawner>().enemyToSpawn = enemyToSpawn;
				tmp.GetComponent<EnemySpawner>().continuous = continuous;
				tmp.GetComponent<EnemySpawner>().randomPosition = randomPosition;
				tmp.GetComponent<EnemySpawner>().spawnCount = spawnCount;
				tmp.GetComponent<EnemySpawner>().spawnDelay = spawnDelay;

				tmp.GetComponent<EnemySpawner>().active = active;
			}
		}
	}

	
	[System.Serializable]
	public class PAmmoTag : Tag
	{
		Dictionary<string, PlayerAmmoTracker.AmmoType> ammoStorage;

		public PAmmoTag(Player p)
		{
			ammoStorage = p.GetComponent<PlayerAmmoTracker>().ammoStorage;
		}
		public void Create()
		{
			PlayerAmmoTracker tmp = GameObject.Find("Player").GetComponent<PlayerAmmoTracker>();
			tmp.ammoStorage.Clear();
			foreach(KeyValuePair<string, PlayerAmmoTracker.AmmoType> kvp in ammoStorage)
			{
				tmp.ammoStorage.Add(kvp.Key,kvp.Value);
			}
		}
	}
}
