﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

/* Basically converts objects to strings, and strings to objects
 * using memory streams and System methods.
 */

public class SerializerBinary
{
	public static BinaryFormatter bf = new BinaryFormatter();
	
	public static string Serialize(object obj)
	{
		MemoryStream ms = new MemoryStream();
		bf.Serialize(ms,obj);
		
		string tmp = System.Convert.ToBase64String(ms.ToArray());
		return tmp;
	}
	
	public static object Deserialize(string serialized)
	{	
		if(serialized != null)
		{
			MemoryStream ms = new MemoryStream(System.Convert.FromBase64String(serialized));
			return bf.Deserialize(ms);
		}
		return null;
	}
}