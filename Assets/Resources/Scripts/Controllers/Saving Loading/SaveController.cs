﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO; 
using System.Text;
using UnityStandardAssets.Characters.FirstPerson;

/*	Handles all saving/loading for the game. Frontend (public) functions
 *  either save/load all loadables/savables, or may do them individually.
 * 
 * 	The backend functions are the generic alternative to having lots of for-loops.
 *  The IO functions just do IO.
 * 
 *  Data is saved in /<ApplicationDir>/SaveData/ using the names specified below.
 *  Data is formatted by line like so:
 * 		<Number of entries in file>
 * 		<String representing entry 1>
 * 		<String representing entry 2>
 * 		...
 * 		<String representing entry n>
 * 		<New Line>
 */

public static class SaveController
{
	//-- HARD-CODED NAMES FOR STUFF, Should probably move this to the Library class.

	public const string saveSubDir = "SaveData";	// I suggest not messing with this, you could beeak everything.	

	public const string saveSubDirCurr = "CURR";	// the sub sub directory for current save data.
	public const string saveSubDirEntry = "ENTRY";	// The sub sub directory for level entry save data.

	public const string saveExt = ".sav";			// The extention of the save file format.

	public const string savPAmmo = "PAMMO";
	public const string savESpawners = "ESPAWNERS";	// The name of the save file for spawner data.
	public const string savWObjects = "WORLDOBJ";	// The name of the save file for world object data.
	public const string savWeapons = "WEAPONS";		// The name of the save file for weapon data.
	public const string savItems = "ITEMS";			// The name of the save file for item data.
	public const string savEnemies = "ENEMIES";		// The name of the save file for enemy data.
	public const string savPlayer = "PLAYER";		// The name of the save file for player data.
	public const string savScene = "SCENE";			// The name of the save file for world data.

	#region FRONTEND

	public static void Save()	// Save Everything - Add new things to be saved when we have them.
	{
		AssembleAndSave<Player,SaveTag.PAmmoTag>(savPAmmo, true);
		//AssembleAndSave<EnemySpawner,SaveTag.ESpawnerTag>(savESpawners, true);
		AssembleAndSave<WorldObject,SaveTag.WObjectTag>(savWObjects, true);
		
		AssembleAndSave<Scene, SaveTag.SceneTag>(savScene, true);
		AssembleAndSave<Weapon,SaveTag.WeaponTag>(savWeapons, true);
		AssembleAndSave<Item,SaveTag.ItemTag>(savItems, true);
		AssembleAndSave<Enemy,SaveTag.EnemyTag>(savEnemies, true);
		AssembleAndSave<Player,SaveTag.PlayerTag>(savPlayer, true);
	}

	public static void Load()	// Load Everything - Add new things to be loaded when we have them.
	{
		ClearAndLoad<Player, SaveTag.PAmmoTag>(savPAmmo, true);
		//ClearAndLoad<EnemySpawner, SaveTag.ESpawnerTag>(savESpawners, true);
		ClearAndLoad<WorldObject, SaveTag.WObjectTag>(savWObjects, true);
		
		ClearAndLoad<Scene, SaveTag.SceneTag>(savScene, true);
		ClearAndLoad<Weapon, SaveTag.WeaponTag>(savWeapons, true);
		ClearAndLoad<Item, SaveTag.ItemTag>(savItems, true);
		ClearAndLoad<Enemy, SaveTag.EnemyTag>(savEnemies, true);

		ClearAndLoad<Player, SaveTag.PlayerTag>(savPlayer, true);
	}

	public static void SaveEnterLevel()	// Save Everything.
	{
		AssembleAndSave<Player, SaveTag.PAmmoTag>(savPAmmo, false);
		//AssembleAndSave<EnemySpawner, SaveTag.ESpawnerTag>(savESpawners, false);
		AssembleAndSave<WorldObject, SaveTag.WObjectTag>(savWObjects, false);
		
		AssembleAndSave<Scene, SaveTag.SceneTag>(savScene, false);
		AssembleAndSave<Weapon,SaveTag.WeaponTag>(savWeapons, false);
		AssembleAndSave<Item,SaveTag.ItemTag>(savItems, false);
		AssembleAndSave<Enemy,SaveTag.EnemyTag>(savEnemies, false);
		AssembleAndSave<Player,SaveTag.PlayerTag>(savPlayer, false);
	}
	
	public static void LoadEnterlevel()	// Load Everything from the entry save.
	{
		// Delete all the things from the current save and load the stuff from the entry save.
		// If we didn't prep the level we'd have duplicate objects in all the levels.

		PrepLevel<Player>(savPAmmo, true); ClearAndLoad<Player, SaveTag.PAmmoTag>(savPAmmo, false);
		//PrepLevel<EnemySpawner>(savESpawners, true); ClearAndLoad<EnemySpawner, SaveTag.ESpawnerTag>(savESpawners, false);
		PrepLevel<WorldObject>(savWObjects, true); ClearAndLoad<WorldObject, SaveTag.WObjectTag>(savWObjects, false);
		
		PrepLevel<Scene>(savScene, true); ClearAndLoad<Scene, SaveTag.SceneTag>(savScene, false);
		PrepLevel<Weapon>(savWeapons, true); ClearAndLoad<Weapon, SaveTag.WeaponTag>(savWeapons, false);
		PrepLevel<Item>(savItems, true); ClearAndLoad<Item, SaveTag.ItemTag>(savItems, false);
		PrepLevel<Enemy>(savEnemies, true); ClearAndLoad<Enemy, SaveTag.EnemyTag>(savEnemies, false);
		
		ClearAndLoad<Player, SaveTag.PlayerTag>(savPlayer, false);
	}

	public static void SaveSingle<ObjectType, TagType>(string saveName, bool curr) 	// Save one thing.
		where ObjectType : Component 
			where TagType : SaveTag.Tag
	{
		AssembleAndSave<ObjectType, TagType>(saveName, curr);
	}

	public static void LoadSingle<ObjectType, TagType>(string saveName, bool curr) 	// Load one thing.
		where ObjectType : Component 
			where TagType : SaveTag.Tag
	{
		ClearAndLoad<ObjectType, TagType>(saveName, curr);
	}

	public static object [] Retrieve(string saveName, bool curr)	// Get the data from a save without loading it.
	{
		string [] data = LoadSav(saveName, curr);
		if(data == null)
		{
			return null;
		}
		
		object [] tags = new object[data.Length];
		for(int i = 0; i < tags.Length; i++)
		{
			tags[i] = SerializerBinary.Deserialize(data[i]);
		}
		return tags;
	}
	#endregion

	#region BACKEND
	//-- Backend things (Generics Everywhere)

	private static void ClearAndLoad<ObjectType, TagType>(string saveName, bool curr) // Clear the level of existing objects of ObjectType and load the data.
		where ObjectType : Component
		where TagType : SaveTag.Tag
	{
		string [] data = LoadSav(saveName, curr);
		if(data == null)
		{
			return;
		}
		
		if(saveName != savPlayer && saveName != savPAmmo)
		{
			ObjectType [] delete = GameObject.FindObjectsOfType<ObjectType>();
			foreach(ObjectType o in delete)
			{
				GameObject.Destroy (o.gameObject);
			}
		}

		TagType [] tags = new TagType[data.Length];

		for(int i = 0; i < tags.Length; i++)
		{
			tags[i] = (TagType)SerializerBinary.Deserialize(data[i]);
		}
		foreach (TagType tt in tags) 
		{
			tt.Create();
		}
	}

	private static void AssembleAndSave<ObjectType, TagType>(string saveName, bool curr) // Get all the objects of type ObjectType and save the data.
		where ObjectType : Component
		where TagType : SaveTag.Tag
	{		
		ObjectType [] objects = GameObject.FindObjectsOfType<ObjectType>();
		string [] objectTags = new string[objects.Length];
		
		for(int i = 0; i < objects.Length; i++)
		{
			object [] parameters = new object[1];
			parameters[0] = objects[i];
			objectTags[i] = SerializerBinary.Serialize(Activator.CreateInstance(typeof(TagType), parameters));
		}
		CreateSav(saveName, objectTags, curr);
	}

	public static void PrepLevel<ObjectType>(string saveName, bool curr) // Remove all objects in the scene with type ObjectType
		where ObjectType : Component
	{
		string [] data = LoadSav(saveName, curr);
		if(data == null)
		{
			return;
		}
		
		if(saveName != savPlayer && saveName != savPAmmo)
		{
			ObjectType [] delete = GameObject.FindObjectsOfType<ObjectType>();
			foreach(ObjectType o in delete)
			{
				GameObject.Destroy (o.gameObject);
			}
		}
	}
#endregion
	#region IO
	//-- FILE IO STUFF --//

	private static void CreateSav (string saveName, string [] data, bool curr)	// Write to file.
	{
		string fileLocation = Application.dataPath+"\\"+saveSubDir; 
		if (curr)
			fileLocation += "\\"+saveSubDirCurr;
		else
			fileLocation += "\\"+saveSubDirEntry;

		string fileName = saveName+saveExt;
		Directory.CreateDirectory(fileLocation);

		StreamWriter writer; 

		FileInfo fi = new FileInfo (fileLocation + "\\" + fileName); 
		if (!fi.Exists)
			writer = fi.CreateText (); 
		else 
		{ 
			fi.Delete (); 
			writer = fi.CreateText (); 
		} 
		
		writer.Write (data.Length+"\n");
		foreach(string s in data)
		{
			writer.Write(s+"\n");
		} 
		writer.Close ();
	}

	private static string [] LoadSav (string saveName, bool curr)	// Read from file.
	{
		string fileLocation = Application.dataPath+"\\"+saveSubDir; 
		if (curr)
			fileLocation += "\\"+saveSubDirCurr;
		else
			fileLocation += "\\"+saveSubDirEntry;

		string fileName = saveName+saveExt;

		if(!File.Exists(fileLocation + "\\" + fileName))
		{
			return null;
		}

		StreamReader r = File.OpenText (fileLocation + "\\" + fileName); 
		int entries;
		if(!int.TryParse(r.ReadLine(),out entries))
		{
			return null;
		}
		string [] data = new string[entries];
		for(int i = 0; i < data.Length; i++)
		{
			data[i] = r.ReadLine();
		}
		r.Close();
		return data;
	}
	#endregion
	#region WIPE
	//-- WIPING STUFF

	public static void WipeSavAll()	// Destroy everything.
	{
		string fileLocation = Application.dataPath+"\\"+saveSubDir; 
		
		if(Directory.Exists(fileLocation))
			Directory.Delete(fileLocation,true);
	}
	
	public static void WipeSavCurr()	// Destroy normal save data.
	{
		string fileLocation = Application.dataPath+"\\"+saveSubDir+"\\"+saveSubDirCurr; 
		
		if(Directory.Exists(fileLocation))
			Directory.Delete(fileLocation,true);
	}
	
	public static void WipeSavEntry()	// Destroy level entry save data.
	{
		string fileLocation = Application.dataPath+"\\"+saveSubDir+"\\"+saveSubDirCurr; 
		
		if(Directory.Exists(fileLocation))
			Directory.Delete(fileLocation,true);
	}
	#endregion
}
