﻿using UnityEngine;
using System.Collections;

// Lighting effect for when a gun is fired. Currently this is only used on the shotgun.
public class MuzzleFlare : MonoBehaviour 
{
	[HideInInspector] public ParticleSystem particle;
	[HideInInspector] public Light flash;

	void Start()
	{
		particle = gameObject.GetComponent<ParticleSystem>();
		flash = gameObject.GetComponent<Light>();

		particle.playOnAwake = false;
		flash.enabled = false;
	}

	public void Flare()
	{
		particle.Play();
		StartCoroutine(Flash ());
	}

	IEnumerator Flash()
	{
		flash.enabled = true;
		yield return new WaitForSeconds(0.01f);
		flash.enabled = false;
	}
}
