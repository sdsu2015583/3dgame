﻿using UnityEngine;
using System.Collections;

// For the flare effect on projectiles. This is so that the sprite is always facing the player so that appears round.
// If the player in the scene isn't actually a player the sprite points at the main camera (This is how it works in the main menu).
public class SpriteLookAtPlayer : MonoBehaviour 
{
	Transform player;
	void Start()
	{
		player = GameObject.Find("Player").transform;
		if(player.GetComponent<CharacterController>() == null)
			player = Camera.main.transform;
	}
	void Update () 
	{
		gameObject.transform.LookAt(player);
	}
}
