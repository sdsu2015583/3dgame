﻿using UnityEngine;
using System.Collections;

// Items rotate along the vertical axis.
public class ItemSpin : MonoBehaviour 
{
	void Update () 
	{
		gameObject.transform.Rotate(Vector3.up);
	}
}
