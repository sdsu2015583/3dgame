﻿using UnityEngine;
using System.Collections;

// This is just a 1 minute script for friday's demo, it has no other use.

public class DemoHandler : MonoBehaviour 
{
	Player p;
	void Start()
	{
		Time.timeScale = 1;
		p = GameObject.Find("Player").GetComponent<Player>();
	}

	void Update()
	{
		if(p != null)
			if(!p.isAlive)
				Application.LoadLevel("MAP_END");
	}

	// I'm honestly just really tired .

	public void MainMenu()
	{
		SaveController.WipeSavAll();
		Application.LoadLevel("MAP_MAINMENU");
	}
}
