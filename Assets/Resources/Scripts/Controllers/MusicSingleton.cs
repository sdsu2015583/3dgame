﻿using UnityEngine;
using System.Collections;

// This class is the music singleton for the
// background music.  Continues between scenes.
public class MusicSingleton : MonoBehaviour 
{
	public float fxVolume  = 1;
	private static MusicSingleton instance;

	void Awake() 
	{
		if (instance != null && instance != this) 
		{
			Destroy( this.gameObject );
			return;
		} 
		else 
		{
			instance = this;
		}
			
		DontDestroyOnLoad( this.gameObject );
	}
		
	// also change this to your script name
	public static MusicSingleton GetInstance() 
	{
		return instance;
	}
}


