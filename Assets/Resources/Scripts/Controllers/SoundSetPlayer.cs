﻿using UnityEngine;
using System.Collections;

// So we don't have to have AudioSources attached to objects (necessarily) for them to play sounds.
// This allows us to delete objects and have a sound play where they were for example.

// Also I got tired of putting PlaySound methods everywhere.

public class SoundSetPlayer : MonoBehaviour 
{
	public static void PlayRandomFromSet(AudioClip [] set, AudioSource source)	// Play a random sound from a set of sounds from a given audiosource.
	{
		source.volume = MusicSingleton.GetInstance().fxVolume;
		int i = Random.Range(0,set.Length);
		source.clip = set[i];
		source.Play();
	}

	public static void PlaySound(AudioSource source, AudioClip clip)	// Play a sound using a given audiosource.
	{
		source.volume = MusicSingleton.GetInstance().fxVolume;
		source.clip = clip;
		source.Play();
	}

	public static void PlaySoundAt(AudioClip clip, Vector3 position)	// Calls PlaySoundAt_Driver, plays a sound at some location.
	{
		GameObject tmp = new GameObject("ItemSound");
		tmp.transform.position = position;
		tmp.transform.parent = GameObject.Find("Items").transform;
		PlaySoundAt_Driver psad = tmp.AddComponent<PlaySoundAt_Driver>();
		psad.PlaySound(clip);
	}

	private class PlaySoundAt_Driver : MonoBehaviour	// Instantiates a sound object, and plays the sound. Deletes when done.
	{
		AudioClip clip;
		public void PlaySound(AudioClip _clip)
		{
			clip = _clip;
			StartCoroutine(Play());
		}
		IEnumerator Play()
		{
			AudioSource source = gameObject.AddComponent<AudioSource>();
			source.volume = MusicSingleton.GetInstance().fxVolume;
			source.clip = clip;
			source.Play();
			yield return new WaitForSeconds(clip.length - .001f);
			Destroy(gameObject);
		}
	}
}
