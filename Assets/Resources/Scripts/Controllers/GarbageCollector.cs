﻿using UnityEngine;
using System.Collections;

// Periodically deletes objects that have been moved under the "Delete" gameobject.

public class GarbageCollector : MonoBehaviour 
{
	GameObject bin;

	public float collectionInterval;

	void Start()
	{
		bin = GameObject.Find("Delete");
	}

	float elapsed;

	void Update () 
	{
		elapsed += Time.deltaTime;
		if(elapsed >= collectionInterval)
		{
			Transform [] tmp = bin.GetComponentsInChildren<Transform>();
			foreach(Transform g in tmp)
			{
				if(g.name != "Delete")
					Destroy(g.gameObject);
			}
			elapsed = 0;
		}
	}
}
