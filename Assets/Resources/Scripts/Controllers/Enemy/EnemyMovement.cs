﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour 
{
	public bool lookAtPlayer;			// Will we always look at the player.
	public bool checkHeight;			// Should we always stay some height above the ground.
	public float minHeight;				// If so how high.

	public Rigidbody rigidBody;
	public float movementMultipler;		// Movement Speed essentially.
	public float followingDistance;		// How close will we will get to the player.
	public float patrolDuration;		// How long will we move in a direction on our patrol route.
	public float persistDuration;		// How long will we continue moving in the player's last known direction after we lose sight of them
	float elapsedPersistDuration = 0;	// How long we've been moving in that direction since we lost sight of them.

	// For shorthand reasons.

	EnemyTrackplayer tracker;
	bool onPatrol = false;

	void Start()
	{
		tracker = gameObject.GetComponent<EnemyTrackplayer>();
		rigidBody = gameObject.GetComponent<Rigidbody>();
	
		rigidBody.useGravity = false;
		rigidBody.isKinematic = false;
		rigidBody.drag = 5f;
		rigidBody.angularDrag = 5f;
		rigidBody.mass = 100f;				// Restrict this so oure movementMultipler makes sense.
		rigidBody.maxAngularVelocity = 1f; 	// Make this configurable or something, maybe maybe not.

		movementMultipler *= 100;
	}

	void Update()
	{
		// If we're currently tracking the player move towards them.
		// If we're currently tracking the player but can't see them because another enemy is in our way, move up as well.
		// If we've lost track of the player move in the direction that we last saw them.
		if(tracker.tracking)
		{
			if(lookAtPlayer)
				gameObject.transform.LookAt(tracker.player.transform);

			if(checkHeight)
				if(tracker.raycaster.CheckHeight(minHeight,transform.position))
					rigidBody.AddForce(Vector3.up * movementMultipler/10);

			if(!tracker.visible)
			{
				rigidBody.AddForce(Vector3.up * movementMultipler/10);
			}

			elapsedPersistDuration = 0;

			if(tracker.distance > followingDistance)
				rigidBody.AddForce(Vector3.ClampMagnitude(tracker.direction,movementMultipler/10) * Time.deltaTime * movementMultipler);
			else
				rigidBody.velocity = Vector3.zero;
		}
		else
		{
			if(elapsedPersistDuration <= persistDuration)
			{
				rigidBody.AddForce(tracker.lastDirection * Time.deltaTime * movementMultipler);
				elapsedPersistDuration = elapsedPersistDuration + Time.fixedDeltaTime;
			}
			else
			{
				if(!onPatrol)
					StartCoroutine(Patrol());
			}
		}
	}

	IEnumerator Patrol()
	{
		// Move left/right/forward/back repeatedly until the player is seen.
		float elapsed = 0;
		int i = Random.Range(0,3);

		// Flipping Right and Forward didn't work so we have all four.
		Vector3 [] patrol = {Vector3.left,Vector3.right,Vector3.forward,Vector3.back};
		
		onPatrol = true;
		while(true)
		{
			if(tracker.tracking)
				break;
			yield return new WaitForFixedUpdate();

			for(; i < 4; i++)
			{
				if(tracker.tracking) 
					break;
				elapsed = 0;
				while(elapsed < patrolDuration)
				{
					if(tracker.tracking) 
						break;
					elapsed = elapsed + Time.fixedDeltaTime;
					rigidBody.AddForce(Vector3.ClampMagnitude((patrol[i] * (movementMultipler / 2)),movementMultipler/10) * elapsed / patrolDuration);
					yield return new WaitForEndOfFrame();
				}
				rigidBody.velocity = Vector3.zero;
			}
			i = 0;
		}
		onPatrol = false;
		yield return new WaitForFixedUpdate();
	}
}
