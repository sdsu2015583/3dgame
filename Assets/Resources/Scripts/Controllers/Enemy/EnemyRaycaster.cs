﻿using UnityEngine;
using System.Collections;

using System.Linq;

public class EnemyRaycaster : MonoBehaviour 
{

	public int filter;

	public Vector3 CastRay(Vector3 origin, Vector3 direction, int damage)
	{
		Ray cast = new Ray(origin,direction);
		RaycastHit [] hits = Physics.RaycastAll(cast,Library.maskEnemyPlayerWorld).OrderBy(h=>h.distance).ToArray();
		
		Debug.DrawLine(cast.origin,cast.GetPoint(100),Color.yellow);
		
		for(int i = 0; i < hits.Length; i++)
		{
			if(hits[i].collider.gameObject.layer == Library.layerPlayer)
			{
				Player tmp = hits[i].collider.gameObject.GetComponent<Player>();
				if(i == 0)	// If the Player is the first thing we hit.
				{
					tmp.OnHit(cast.direction, damage);
				}
				if(i == 1)	// If the Player is the second thing we hit, and the first thing we hit wasn't an enemy.
				{
					if(hits[i-1].collider.gameObject.GetComponent<Shell>() != null)
						break;
					tmp.OnHit(cast.direction, damage);
				}
			}
			if( i== 1 )	// If the player wasn't the second thing we hit the player isn't visible.
				break;
		}
		return cast.direction;
	}

	public bool IsPlayerVisible(Vector3 origin, Vector3 direction, bool filtered)
	{
		// A filtered check should disregard other enemies.
		// An unfiltered check should check for enemies blocking our path.
		if (Time.timeScale != 0) 
		{
			if(filtered)
			{
				Ray cast = new Ray(origin,direction);
				RaycastHit hit = new RaycastHit();
				
				if(Physics.Raycast(cast,out hit,Mathf.Infinity,Library.maskPlayerWorld))
				{
					if(hit.collider.gameObject.layer == Library.layerPlayer)
						return true;
				}
			}
			else
			{
				Ray cast = new Ray(origin,direction);
				RaycastHit [] hits = Physics.RaycastAll(cast,Library.maskEnemyPlayerWorld).OrderBy(h=>h.distance).ToArray();

				for(int i = 0; i < hits.Length; i++)
				{
					if(hits[i].collider.gameObject.layer == Library.layerPlayer)
					{
						if(i == 0)	// If the Player is the first thing we hit.
						{
							return true;
						}

						if(i == 1)	// If the Player is the second thing we hit, and the first thing we hit wasn't an enemy.
						{
							if(hits[i-1].collider.gameObject.GetComponent<Shell>() != null)
								return true;
							return false;
						}
					}
					if( i== 1 )	// If the player wasn't the second thing we hit the player isn't visible.
						break;
				}
			}
		}
		return false;
	}

	// Raycast down to see how high we are. This might get weird with the elite enemies, but it's fine.
	public bool CheckHeight(float height, Vector3 origin)
	{
		if(Time.timeScale != 0)
		{
			Ray down = new Ray();
			RaycastHit hit = new RaycastHit();
			
			down.origin = origin;
			down.direction = Vector3.down;
			
			Debug.DrawRay(down.origin,down.direction);
			
			int layerMask = 1 << filter;
			if(Physics.Raycast(down,out hit,Mathf.Infinity,~layerMask))
			{
				if (hit.distance < height)
					return true;
			}
		}
		return false;
	}
}
