﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This controls the shielding and increasing aggression behavior for Elite enemies.
public class EnemyEliteCoreControl : MonoBehaviour 
{
	EnemyWeapon weapon;
	Enemy core;
	bool rotating;
	Light coreLight;

	public bool switchShields;

	public float switchShieldInterval;
	public float rateOfFireInitial;
	public float rateOfFireBump;

	[HideInInspector] public List<Shell> shells;

	Shell shield;

	void Start()
	{
		weapon = gameObject.GetComponent<EnemyWeapon>();
		core = gameObject.GetComponent<Enemy>();
		shells = new List<Shell>(gameObject.GetComponentsInChildren<Shell>());
		
		PickShield();

		// Effects		
		coreLight = gameObject.AddComponent<Light>();
		coreLight.range = 20;
		coreLight.intensity = .5f;
		coreLight.shadows = LightShadows.Hard;
	}

	// Randomly pick one of our shells to be our shield
	public void PickShield()
	{
		shield = shells[Random.Range(0,shells.Count)];
	}

	// To be called in the Shell script, to remove themselves from the list when they are destroyed.
	public void RemoveShell(Shell s)
	{
		if(s == shield) shield = null;
		shells.Remove(s);
	}

	float elapsed;

	// Rotate our shield towards the player to protect ourselves.
	void Update()
	{
		if(shells.Count != 0)
		{
			elapsed += Time.deltaTime;
			if(shield == null)
				PickShield();
			Ray rayShield = new Ray(shield.transform.position,shield.shieldPoint.position - shield.transform.position);
			Debug.DrawLine(rayShield.origin,rayShield.GetPoint(10),Color.blue);

			Ray rayPlayer = new Ray(shield.transform.position,core.control.tracker.lastDirection);
			Debug.DrawLine(rayPlayer.origin,rayPlayer.GetPoint(10),Color.red);

			Vector3 alpha = rayShield.direction;
			Vector3 beta = rayPlayer.direction;

			Vector3 delta = Vector3.Cross(alpha,beta);

			//transform.Rotate(delta * Time.deltaTime * 100);

			if (Mathf.Abs(Vector3.Angle(alpha,beta)) > 1f)
			{
				transform.RotateAround(transform.position,delta,Time.deltaTime * 100);
			}


			if(elapsed >= switchShieldInterval)
			{
				if(switchShields)
				{
					elapsed = 0;
					PickShield();
				}
			}
		}
		if(core.isAlive == false)
		{
			Destroy(coreLight);
			for(int i = shells.Count-1; i >= 0; i--)
			{
				shells[i].Kill();
			}
			Destroy(this);
		}
	}

	// As we lose shells increase the rate of fire.
	void FixedUpdate()
	{
		if(Time.timeScale != 0)
		{
			switch(shells.Count)
			{
				case 8:
				weapon.SetRateOfFire(rateOfFireInitial);
				break;
				case 6:
				weapon.SetRateOfFire(rateOfFireInitial + rateOfFireBump * 1);
				break;
				case 4:
				weapon.SetRateOfFire(rateOfFireInitial + rateOfFireBump * 2);
				break;
				case 2:
				weapon.SetRateOfFire(rateOfFireInitial + rateOfFireBump * 3);
				break;
				case 1:
				weapon.SetRateOfFire(rateOfFireInitial + rateOfFireBump * 4);
				break;
			}
		}
	}
}
