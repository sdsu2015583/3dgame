﻿using UnityEngine;
using System.Collections;

public class EnemyTrackplayer : MonoBehaviour 
{
	Vector3 enemyOrigin;

	[HideInInspector] public float distance;		// Distance to the player.
	[HideInInspector] public Vector3 direction;		// Direction to player.
	[HideInInspector] public Vector3 lastDirection;	// Direction to last known location of player.
	[HideInInspector] public bool tracking;			// Can this enemy see the player (Ignoring Enemies).
	[HideInInspector] public bool visible;			// Can this enemy see the player (Enemies Included).

	[HideInInspector] public Player player;
	[HideInInspector] public EnemyRaycaster raycaster;

	void Start()
	{
		raycaster = gameObject.GetComponent<EnemyRaycaster>();
		player = GameObject.Find("Player").GetComponent<Player>();

		visible = false;
	}

	// If we can see the player track the player. Record their last direction so we know where they were if we lost sight of them.
	void Update()
	{
		if (player == null) GameObject.Find("Player").GetComponent<Player>();
		if( player.isAlive && Time.timeScale != 0)
		{
			if(raycaster.IsPlayerVisible(enemyOrigin,direction,false))
			   visible = true;
			else
				visible = false;
			  
			distance = Vector3.Distance(enemyOrigin, player.transform.position);

			enemyOrigin = transform.position;
			direction = player.transform.position - enemyOrigin;

			if(raycaster.IsPlayerVisible(enemyOrigin,direction,true))
			{
				tracking = true;
				lastDirection = direction;
				Ray sightLine = new Ray (enemyOrigin,lastDirection);
				Debug.DrawLine(sightLine.origin,sightLine.GetPoint(distance));
			}
			else
				tracking = false;
		}
		else
			tracking = false;
	}
}
