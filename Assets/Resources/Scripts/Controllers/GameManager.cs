using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	void Start()
	{
		Physics.IgnoreLayerCollision(2,2);

		// So the game doesn't start out frozen when we load into this level.
		Application.targetFrameRate = 60;
		GameObject.Find ("UIController_InGame").GetComponent<UIController_InGame>().Unpause();


		// Handle previous savedata. We could've solved this with a singleton script but it's too late.
		object [] currData = SaveController.Retrieve(SaveController.savScene, true);
		object [] entryData = SaveController.Retrieve(SaveController.savScene, false);

		if(currData != null)
		{
			Debug.Log(" Found previous save data. ");
			SaveTag.SceneTag sceneData = (SaveTag.SceneTag) currData[0];

			if(sceneData.level == Application.loadedLevelName)
			{
				Debug.Log (" Data is for current level. ");
				if(sceneData.fromLevel)
				{
					Debug.Log ("Coming from a level transition, this is not a resume");
					Debug.Log ("The player location will not be loaded.");

					// Only load the player data, so we don't wipe the map.
					GameObject.Find("SceneMarker").GetComponent<Scene>().fromLevel = true;
					SaveController.LoadSingle<Player, SaveTag.PlayerTag>(SaveController.savPlayer, true);
					SaveController.LoadSingle<Player, SaveTag.PAmmoTag>(SaveController.savPAmmo, true);
					GameObject.Find("SceneMarker").GetComponent<Scene>().fromLevel = false;

					SaveController.SaveEnterLevel();
					SaveController.Save();

					return;
				}
				else
				{
					Debug.Log ("Not coming from a level transition, this is a resume");
					Debug.Log ("The player location will be loaded.");

					SaveController.Load();

					return;
				}
			}
			Debug.Log (" Data is for a different level.");
			
			SaveController.SaveEnterLevel();
			SaveController.Save();
			
			return;
		}
		Debug.Log (" Data does not exist. ");

		SaveController.SaveEnterLevel();
		SaveController.Save();
	}

	public void Pause()
	{
		Time.timeScale = 0;
	}
	public void Unpause()
	{
		Time.timeScale = 1;
	}

	public void NewGame()
	{
		SaveController.WipeSavAll();
		Application.LoadLevel("MAP_000"); // This should be the first map because of our naming scheme.
	}

	public void MainMenu()
	{
		Application.LoadLevel("MAP_MAINMENU"); // This should be the main menu because of our naming scheme.
	}

	public void ReloadLastSave()
	{
		SaveController.Load();
		ClearGarbage();
		GameObject.Find ("UIController_InGame").GetComponent<UIController_InGame>().Unpause();
	}

	public void RestartLevel()
	{
		SaveController.WipeSavCurr();
		SaveController.Load();

		SaveController.LoadEnterlevel();
		ClearGarbage();
		GameObject.Find ("UIController_InGame").GetComponent<UIController_InGame>().Unpause();

		// This may be necessary, but it's been unstable lately.
		//Application.LoadLevel(Application.loadedLevelName);

	}

	public void SaveAndQuit()
	{
		SaveController.Save();
		MainMenu();
	}

	public void ClearGarbage()
	{
		// Band-aid fix - Force the deletion of all garbage objects.
		Transform [] g = GameObject.Find("Garbage").GetComponentsInChildren<Transform>();
		foreach(Transform t in g)
		{
			if(t.gameObject.name != "Garbage")
			{
				Destroy (t.gameObject);
			}
		}
	}

	//-- THESE ARE FOR TESTING PURPOSES.

	public void SaveNow()
	{
		SaveController.Save();
	}

	public void WipeSave()
	{
		SaveController.WipeSavAll();
		Application.LoadLevel(Application.loadedLevelName);
	}
}

