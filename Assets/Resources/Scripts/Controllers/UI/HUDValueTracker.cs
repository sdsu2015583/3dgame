﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Tracks important values and updates text displays on screen.
public class HUDValueTracker : MonoBehaviour 
{
	public Text textHealth;
	public Text textArmor;
	public Text textHasObjective;
	public Text textAmmo;

	Player player;

	void Awake()
	{
		player = GameObject.Find ("Player").GetComponent<Player>();
	}

	void Update () 
	{
		textHealth.text = "HEALTH: " + player.health;
		textArmor.text = "ARMOR: " + player.armor;
		textHasObjective.text = "HAS OBJECTIVE: " + player.hasObjective;
		UpdateAmmo();
	}

	void UpdateAmmo()
	{
		// Gets the ammoStorage dictionary in PlayerAmmoTracker and lists every key and value.
		textAmmo.text = "";
		foreach(KeyValuePair<string,PlayerAmmoTracker.AmmoType> entry in player.GetComponent<PlayerAmmoTracker>().ammoStorage)
		{
			textAmmo.text = textAmmo.text + "\n Ammo: " + entry.Key + " = " + entry.Value.count;
		}
	}
}
