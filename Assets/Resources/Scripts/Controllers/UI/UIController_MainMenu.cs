﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Controls canvas swapping in the main menu, and handles loading from playerprefs for volume options.

public class UIController_MainMenu : MonoBehaviour 
{
	public string firstLevel;
	public string currentLevel;

	public Canvas canvasMain;
	public Canvas canvasOptions;
	public Canvas canvasCredits;
	public Canvas canvasGlobal;

	void Start()
	{
		Time.timeScale = 1;
		CloseAllMenus();
		SwitchCanvas(canvasMain, true);

		Slider [] sliders = canvasOptions.GetComponentsInChildren<Slider>();

		for(int i = 0; i < sliders.Length; i++)
		{
			if (sliders[i].name.Contains("Music"))
			{
				if(PlayerPrefs.HasKey("SliderVolumeMusicAmbience"))
					GameObject.Find("MUSSIC").GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("SliderVolumeMusicAmbience");
			}

			if (sliders[i].name.Contains("Effects"))
			{
				if(PlayerPrefs.HasKey("SliderVolumeSoundEffects"))
					GameObject.Find("MUSSIC").GetComponent<MusicSingleton>().fxVolume = PlayerPrefs.GetFloat("SliderVolumeSoundEffects");
			}
		}
	}

	public void NewGame()
	{
		SaveController.WipeSavAll();
		Application.LoadLevel(firstLevel);
	}
	public void LoadGame()
	{
		object [] data = SaveController.Retrieve("SCENE", true);
		if(data != null)
		{
			Application.LoadLevel(((SaveTag.SceneTag)data[0]).level);
		}

		// else there is no saved game, tell the player somehow.
	}
	public void Quit()
	{
		Application.Quit();
	}

	public void Options()
	{
		CloseAllMenus();
		SwitchCanvas(canvasOptions, true);
		SwitchCanvas(canvasGlobal, true);

		Slider [] sliders = canvasOptions.GetComponentsInChildren<Slider>();
		
		for(int i = 0; i < sliders.Length; i++)
		{
			print (sliders[i].name);
			if (sliders[i].name.Contains("Music"))
				sliders[i].value = PlayerPrefs.GetFloat(sliders[i].name);
			if (sliders[i].name.Contains("Effects"))
				sliders[i].value = PlayerPrefs.GetFloat(sliders[i].name);
		}

		// We really don't need to use another scene.
		// Application.LoadLevel("Options");
	}

	public void Credits()
	{
		CloseAllMenus();
		SwitchCanvas(canvasCredits, true);
		SwitchCanvas(canvasGlobal, true);
	}

	public void Back()
	{
		CloseAllMenus();
		SwitchCanvas(canvasMain, true);
	}

	public void WipeSave()
	{
		SaveController.WipeSavAll();
	}

	public void Apply()
	{
		Slider [] sliders = canvasOptions.GetComponentsInChildren<Slider>();

		foreach(Slider s in sliders)
		{
			if (s.name.Contains("Music"))
			{
				print (s.value);
				GameObject.Find("MUSSIC").GetComponent<AudioSource>().volume = s.value;
				PlayerPrefs.SetFloat(s.name,s.value);
			}
			if (s.name.Contains("Effects"))
			{
				print(s.value);
				GameObject.Find("MUSSIC").GetComponent<MusicSingleton>().fxVolume = s.value;
				PlayerPrefs.SetFloat(s.name,s.value);
			}
		}
	}

	//-- CANVAS OPERATIONS

	public void CloseAllMenus()
	{
		Canvas [] canvases = gameObject.GetComponentsInChildren<Canvas>();
		foreach(Canvas c in canvases)
		{
			SwitchCanvas(c, false);
		}
	}

	void SwitchCanvas(Canvas c, bool flag)
	{
		c.enabled = flag;
		SwitchComponents(c,flag);
	}
	
	void SwitchComponents(Canvas c, bool flag)
	{
		Selectable [] components = c.GetComponentsInChildren<Selectable>();
		foreach(Selectable s in components)
		{
			s.enabled = flag;
		}
	}
}


