﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Handles the pause menu in-game. Toggles canvases/components.

public class UIController_InGame : MonoBehaviour 
{
	private bool paused = false;

	public Canvas hud;
	public Canvas pauseMenu;
	public Canvas deathMenu;

	void Start()
	{
		Cursor.visible = false;
		CloseAll();
		SwitchCanvas(hud, true);
	}

	void Update() 
	{
		// Keep the cursor in the center. If Esc is pressed allow the cursor to escape.
		if(!paused)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.lockState = CursorLockMode.None;
		}
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			// If we force the player to use the RESUME button then we have the added benifit of bringing the game window back into focus automatically.
			// Otherwise the player would have to click again to refocus the mouse. There's probably a workaround but I haven't found it.			
			if(!paused)
			{
				Pause ();
			}
		}
	}

	void CloseAll()
	{
		SwitchCanvas(hud,false);
		SwitchCanvas(pauseMenu,false);
		SwitchCanvas(deathMenu,false);
	}

	public void Pause()
	{
		paused = true;
		// Hide the HUD, show the menu, tell the GameManager to pause the game.
		GameObject.Find("GameManager").GetComponent<GameManager>().Pause();
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		GUI.FocusWindow(0);

		CloseAll ();
		SwitchCanvas(pauseMenu,true);
	}

	public void PauseDeath()
	{
		paused = true;
		// Hide the HUD, show the menu, tell the GameManager to pause the game.
		GameObject.Find("GameManager").GetComponent<GameManager>().Pause();
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		GUI.FocusWindow(0);
		
		CloseAll ();
		SwitchCanvas(deathMenu,true);
	}
	
	public void Unpause()
	{
		paused = false;
		// Show the HUD, hide the menu, tell the GameManager to unpause the game.
		GameObject.Find("GameManager").GetComponent<GameManager>().Unpause();
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		GUI.FocusWindow(0);

		CloseAll ();
		SwitchCanvas(hud,true);
	}

	// Apparently just turning off the canvas isn't enough, because the keyboard still triggers the buttons.
	// So we also disable all the buttons with the SwitchButtons.

	void SwitchCanvas(Canvas c, bool flag)
	{
		c.enabled = flag;
		SwitchButtons(c,flag);
	}

	void SwitchButtons(Canvas c, bool flag)
	{
		Button [] buttons = c.GetComponentsInChildren<Button>();
		foreach(Button b in buttons)
		{
			b.enabled = flag;
		}
	}
}
