﻿using UnityEngine;
using System.Collections;

// Handles player transitions between levels using the warp panel.

public class NextLevelPlatform : MonoBehaviour 
{
	public AudioClip soundTransition;
	public string nextLevel;

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.layer == Library.layerPlayer)
			StartCoroutine(Transition());
	}

	IEnumerator Transition()
	{
		SoundSetPlayer.PlaySoundAt(soundTransition,transform.position);
		yield return new WaitForSeconds(.5f);
		SaveController.WipeSavAll();

		GameObject.Find("Player").GetComponent<Player>().hasObjective = false;
		GameObject.Find("SceneMarker").GetComponent<Scene>().fromLevel = true;
		GameObject.Find("SceneMarker").GetComponent<Scene>().level = nextLevel;

		// This is the only data we care about, everything else is garbage relative to the next level.

		SaveController.SaveSingle<Scene, SaveTag.SceneTag>(SaveController.savScene, true);
		SaveController.SaveSingle<Player, SaveTag.PlayerTag>(SaveController.savPlayer, true);
		SaveController.SaveSingle<Player, SaveTag.PAmmoTag>(SaveController.savPAmmo, true);

		Application.LoadLevel(nextLevel);
	}
}
