﻿using UnityEngine;
using System.Collections;

// Doors, open when used.

public class Door : WorldObject ,Triggerable
{
	Color oldColor;
	Color newColor;
	AudioSource soundSource;

	Light lightGlow;

	public Triggerable triggerTarget;
	public AudioClip soundFade;

	bool inTransition;

	void Start()
	{
		active = true;
		soundSource = gameObject.AddComponent<AudioSource>();
		lightGlow = gameObject.AddComponent<Light>();


		// Save the original color.
		Color tmp = GetComponent<MeshRenderer>().material.color;
		oldColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);
		newColor = new Color (Color.green.r, Color.green.g, Color.green.b, 0f);

		lightGlow.color = oldColor;
		GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
		GetComponent<Renderer>().material.SetColor("_EmissionColor", oldColor);
	}

	IEnumerator FadeOut(float duration)
	{
		inTransition = true;
		active = false;
		SoundSetPlayer.PlaySound(soundSource,soundFade);

		float elapsed = 0;		
		while(elapsed < duration)
		{
			if(elapsed >= duration/2)
				gameObject.GetComponent<Collider> ().enabled = false;
			GetComponent<MeshRenderer>().material.color = Color.Lerp(oldColor,newColor,elapsed/duration);
			elapsed = elapsed + Time.fixedDeltaTime;
			yield return new WaitForEndOfFrame();
		}
		GetComponent<Renderer> ().enabled = false;
		active = false;
		inTransition = false;
	}

	public void Trigger()
	{
		// Fade out and trigger the linked triggerable object (not necessary).
		StartCoroutine (FadeOut (soundFade.length/2));
		if(triggerTarget != null)
			triggerTarget.Trigger();
	}

	public override void Reset()
	{
		active = true;
		GetComponent<Renderer> ().enabled = true;
		gameObject.GetComponent<Collider> ().enabled = true;
		GetComponent<MeshRenderer>().material.color = oldColor;
	}

	public override void Disable()
	{
		active = false;
		gameObject.GetComponent<Collider> ().enabled = false;
		GetComponent<Renderer> ().enabled = false;
	}
}
