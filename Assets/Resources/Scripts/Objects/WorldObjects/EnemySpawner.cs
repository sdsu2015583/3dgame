﻿using UnityEngine;
using System.Collections;

// Spawns some enemy at one of the points in the prefab.

public class EnemySpawner : MonoBehaviour, Triggerable
{
	public string enemyToSpawn;		// String name of enemy to spawn.

	public bool startOn;			// Does this start spawning as soon as the level is loaded.

	public bool continuous;			// Unlimited spawning.
	public bool randomPosition;		// Choose a random position amongst the positions.
	public int spawnCount;			// Number of enemies to spawn if this isn't continuous.
	public float spawnDelay;		// How long to wait between spawns.

	public Transform[] spawnPoints; 
	GameObject enemy;
	public GameObject altEnemy;		// If the resource can't be loaded use this enemy instead.

	[HideInInspector] public bool active = false;

	void Start()
	{
		enemy = Resources.Load("Prefabs/Actors/" + enemyToSpawn,typeof(GameObject)) as GameObject;
		if(startOn)
			Init ();
	}

	public void Init()
	{
		enemy = Resources.Load("Prefabs/Actors/" + enemyToSpawn,typeof(GameObject)) as GameObject;
		active = true;
	}

	float elapsed = 0;
	int spawned = 0;
	int spawnPointIndex = 0;

	void Update()
	{
		if(enemy == null) enemy = altEnemy;
		if(active && Time.timeScale != 0)
		{
			if (elapsed >= spawnDelay)
			{
				if(randomPosition) spawnPointIndex = Random.Range (0, spawnPoints.Length);

				Instantiate (enemy, spawnPoints [spawnPointIndex].position, new Quaternion());

				elapsed = 0;
				spawnPointIndex++;
				spawned++;
			}
			else
				elapsed += Time.deltaTime;

			if(continuous) spawned = 0;
			if(spawned == spawnCount) { active = false; }
			if(spawnPointIndex == 4) spawnPointIndex = 0;
		}
	}

	public void Trigger()
	{
		Kill ();
	}

	public void Kill()
	{
		Destroy (gameObject);
	}
}
