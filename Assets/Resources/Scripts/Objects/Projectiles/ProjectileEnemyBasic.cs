﻿using UnityEngine;
using System.Collections;

// This is for enemy projectiles. Using this in a player intended projectile would just kill the player as they fire.

public class ProjectileEnemyBasic : Projectile 
{
	public override void OnContact(Collider col)
	{
		if(col.gameObject.layer != Library.layerEnemy) // If whatever we hit wasn't a fellow enemy.
		{
			// If we hit a player do damage to the player, if we hit a weapon do damage to the player holding the weapon.
			Player p = col.gameObject.GetComponent<Player>();
			if(p != null)
			{
				p.OnHit(direction, projectileDamage);
				Destroy(gameObject);
			}
			else
			{
				Weapon w = col.gameObject.GetComponent<Weapon>();
				if(w != null)
				{
					p = w.player.GetComponent<Player>();
					if(p != null)
					{
						p.OnHit(direction, projectileDamage);
						Destroy(gameObject);
					}
				}
			}
		}
	}
}
