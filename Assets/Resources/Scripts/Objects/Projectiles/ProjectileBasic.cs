﻿using UnityEngine;
using System.Collections;

public class ProjectileBasic : Projectile 
{
	// This is strictly for player weapons, if you're use it elsewhere and things don't work I told you so.

	public override void OnContact(Collider col)
	{
		if(col.gameObject.layer != Library.layerPlayer) // If whatever we hit wasn't the player.
		{
			Hittable tmp = col.gameObject.GetComponent<Hittable>();
			if(tmp != null)
			{
				tmp.OnHit(direction, projectileDamage);
				Destroy (gameObject);
			}
			if(col.gameObject.GetComponent<Weapon>() == null) // Don't interact with the front of the gun.
			{
				Destroy(gameObject);
			}
		}
	}
}
