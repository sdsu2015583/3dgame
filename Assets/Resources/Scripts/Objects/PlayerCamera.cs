﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

// For recoil.

public class PlayerCamera : MonoBehaviour 
{
	GameObject player;

	void Start()
	{
		player = GameObject.Find("Player");
	}
	public void KickView(float amount) // Move the view vertically by some degree.
	{
		Quaternion rotation = gameObject.transform.localRotation;
		rotation.x -= amount;

		gameObject.transform.localRotation = rotation;
		player.GetComponentInChildren<FirstPersonController>().m_MouseLook.Init(player.transform,player.GetComponentInChildren<Camera>().transform);
	}
}
