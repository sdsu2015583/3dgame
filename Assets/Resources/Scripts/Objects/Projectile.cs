﻿using UnityEngine;
using System.Collections;

// Base projectile class.
// Can be used for both player and enemy projectiles.

public class Projectile : MonoBehaviour 
{
	[HideInInspector] public Vector3 direction;
	[HideInInspector] public float elapsedTime = 0;
	[HideInInspector] public int projectileDamage;

	public float movementMultipler;
	public float projectileLifetime;

	void Start()
	{
		// So we don't make a mess of the heirarchy.
		if(GameObject.Find("Projectiles") != null)
			gameObject.transform.parent = GameObject.Find("Projectiles").transform;
	}

	public void Init(Vector3 _direction, int damage)
	{
		direction = _direction; 
		projectileDamage = damage;
	}

	void Update()
	{
			if(elapsedTime < projectileLifetime)
			{
				elapsedTime = elapsedTime + Time.deltaTime;
				gameObject.transform.Translate(direction * Time.deltaTime * movementMultipler);
			}
			else
				Destroy (gameObject);
	}

	void OnTriggerEnter( Collider col )
	{
		OnContact(col);
	}

	public virtual void OnContact(Collider col)
	{
		// Override this.
	}
}
