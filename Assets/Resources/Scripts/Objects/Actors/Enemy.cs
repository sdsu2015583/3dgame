﻿using UnityEngine;
using System.Collections;

// General enemy variant of actor, the movement and tracking properties of all enemies.
// Customizable per enemy in inspector.

public class Enemy : Actor 
{
	Color oldColor;
	public string enemyType;

	[System.Serializable]
	public class Sound	// Group this stuff in the inspector
	{
		public float aggroPulseDelay;
		public float aggroPulseDelayVariance;
		public float aggroPulseVolumeRatio;
		
		[Range(0f,2f)] public float volume;

		public AudioClip Death;		// Heath sound.
		public AudioClip Aggro;		// Sound to play while the enemy is engaging the player.
		public AudioClip [] SetImpact;	// Hurt sounds.
		
		[HideInInspector] public AudioSource sourceGeneral;
		[HideInInspector] public AudioSource sourceHit;
		[HideInInspector] public AudioSource sourceAggro;

		public void Init(GameObject obj)
		{
			sourceGeneral = obj.AddComponent<AudioSource>();
			sourceHit = obj.AddComponent<AudioSource>();
			sourceAggro = obj.AddComponent<AudioSource>();
			
			sourceGeneral.volume = sourceHit.volume = volume;
			sourceAggro.volume = aggroPulseVolumeRatio * volume;
			sourceGeneral.priority = 129;
			sourceHit.priority = 130; 
			sourceAggro.priority = 131;
		}
	}

	[System.Serializable]
	public class Control	// Group this stuff in the inspector
	{
		[HideInInspector] public EnemyTrackplayer tracker;
		[HideInInspector] public EnemyRaycaster raycaster;
		[HideInInspector] public EnemyMovement movement;


		public bool lookAtPlayer;
		public bool checkHeight;

		public float minimumHeight;
		public float movementMultipler;
		public float followingDistance;
		public float patrolDuration;
		public float persistDuration;

		public void Init(GameObject obj)
		{
			tracker = obj.AddComponent<EnemyTrackplayer>();
			raycaster = obj.AddComponent<EnemyRaycaster>();
			movement = obj.AddComponent<EnemyMovement>();

			movement.checkHeight = checkHeight;
			movement.minHeight = minimumHeight;
			movement.lookAtPlayer = lookAtPlayer;
			movement.movementMultipler = movementMultipler;
			movement.followingDistance = followingDistance;
			movement.patrolDuration = patrolDuration;
			movement.persistDuration = persistDuration;
		}
	}

	public Sound sound;
	public Control control;

	bool aggro;

	void Start () 
	{
		isAlive = true;
		StartCoroutine(ActorUpdate()); // Inherited update. Because we replaced update.

		// So we don't make a mess of the heirarchy.
		if(GameObject.Find("Enemies") != null)
			gameObject.transform.parent = GameObject.Find("Enemies").transform;

		sound.Init(gameObject);
		control.Init(gameObject);

		// Effects Stuff
		Color tmp = GetComponentInChildren<MeshRenderer>().material.color;
		this.gameObject.GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
		oldColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);
	}

	void Update() // If we're tracking the player play the aggro noise.
	{
		if(control.tracker.tracking)
		{
			if(!aggro)
				StartCoroutine(Aggro());
		}
	}

	public override void Kill()	// Move this enemy to garbage and disable everything that makes it work, the mark it for deletion.
	{
		GameObject tmp = GameObject.Find("Garbage");
		if(tmp != null)
			transform.parent = tmp.transform;
		else
			transform.parent = null;

		SoundSetPlayer.PlaySoundAt(sound.Death,gameObject.transform.position);
		
		control.movement.rigidBody.useGravity = true;
		control.movement.rigidBody.isKinematic = false;
		control.movement.rigidBody.mass = 100;
		control.movement.rigidBody.angularDrag = 0;
		control.movement.rigidBody.drag = 0;

		Destroy(control.movement);
		Destroy(control.tracker);
		Destroy(control.raycaster);

		oldColor = Color.black;
		StartCoroutine(LerpBack(Color.red,1f,true));
	}

	public override void OnHit(Vector3 dir, int value) // Play a hit sound and take damage.
	{
		if(value != 0)
		{
			SoundSetPlayer.PlayRandomFromSet(sound.SetImpact,sound.sourceHit);
			base.OnHit (dir,value);
		}
	}

	IEnumerator Aggro()	// Repeatedly play the specified sound.
	{
		aggro = true;
		while (true)
		{
			if(!control.tracker.tracking || !gameObject.GetComponent<Actor>().isAlive) break;
			PlaySound(sound.sourceAggro, sound.Aggro);
			yield return new WaitForSeconds(sound.aggroPulseDelay+(1/Random.Range(0,sound.aggroPulseDelayVariance-1)+1));
		}
		aggro = false;
	}

	IEnumerator LerpBack(Color newColor,float duration, bool end)	// Fancy effects.
	{
		// Transition from red to oldColor in 'duration' seconds.
		float elapsed = 0;
		while(elapsed < duration)
		{
			GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(newColor,Color.black,elapsed/duration));
			GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(newColor,oldColor,elapsed/duration);
			elapsed = elapsed + Time.fixedDeltaTime;
			yield return new WaitForEndOfFrame();
		}
		if(end)
		{
			StartCoroutine(MarkForDelete());
			Destroy(this);
		}
	}
}
