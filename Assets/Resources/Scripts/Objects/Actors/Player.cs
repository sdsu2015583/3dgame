﻿using UnityEngine;
using System.Collections;

// Variant of Actor for the player.
// Tracks if the player has the objective item, and processes Use input.

public class Player : Actor 
{
	public float useReach;
	[HideInInspector] public bool hasObjective;
	[HideInInspector] public PlayerAmmoTracker ammoTracker;

	void Start()
	{
		ammoTracker = gameObject.GetComponent<PlayerAmmoTracker>();

		isAlive = true;
		StartCoroutine (ActorUpdate ());
		PlayerPrefs.SetFloat ("health", getHealth());
		PlayerPrefs.SetFloat ("armor", getArmor());
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.E))
			Use ();
	}

	public override void Kill()
	{
		print ("YOU DIED");
		GameObject.Find("UIController_InGame").GetComponent<UIController_InGame>().PauseDeath();
	}

	void Use()
	{
		Transform cam = GetComponentInChildren<Camera> ().gameObject.transform;
		Ray forward = new Ray (cam.position, cam.forward);
		RaycastHit hit = new RaycastHit ();

		int layermask = (1 << Library.layerWorld);
		if(Physics.Raycast(forward,out hit,Mathf.Infinity,layermask))
		{
			if(Vector3.Distance(hit.point,gameObject.transform.position) <= useReach)
			{
				Triggerable tmp = hit.collider.gameObject.GetComponent<Triggerable>();
				if(tmp != null)
					tmp.Trigger();
			}
		}
	}

}
