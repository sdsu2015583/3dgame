﻿using UnityEngine;
using System.Collections;

// Actor variant for the shell objects on Elite Enemies, Using Enemy for this wasn't appropriate because
// I didn't want them to move individually.
// When shells die they tell the core object they've died, they're then removed from the active shells pool.

public class Shell : Actor
{
	Enemy core;
	EnemyEliteCoreControl coreControl;
	Rigidbody rigidBody;

	Color oldColor;

	public AudioClip [] shellDie;
	public Transform shieldPoint;

	void Start()
	{
		isAlive = true;

		rigidBody = gameObject.GetComponent<Rigidbody>();
		if(gameObject.transform.parent.name == "Shells")
		{
			core = gameObject.transform.parent.transform.parent.gameObject.GetComponent<Enemy>();
			coreControl = core.gameObject.GetComponent<EnemyEliteCoreControl>();
		}

		rigidBody.useGravity = false;
		rigidBody.isKinematic = true;
		rigidBody.mass = 200;	// So a dying core doesn't make these fly away.

		Physics.IgnoreCollision(gameObject.GetComponent<Collider>(),core.gameObject.GetComponent<Collider>());

		// Effects Stuff
		Color tmp = GetComponentInChildren<MeshRenderer>().material.color;
		this.gameObject.GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
		oldColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);

		StartCoroutine(ActorUpdate());
	}
	
	public override void OnHit(Vector3 dir, int value)	// Take damage, play noises.
	{
		if(isAlive)
		if(value != 0)
		{
			StartCoroutine(LerpBack(Color.yellow,.5f,false));
			SoundSetPlayer.PlayRandomFromSet(core.sound.SetImpact,core.sound.sourceHit);
			base.OnHit (dir,value);
		}
	}

	public override void Kill()	// Move to garbage and make simple rigidbody.
	{
		GameObject tmp = GameObject.Find("Garbage");
		if(tmp != null)
			transform.parent = tmp.transform;
		else
			transform.parent = null;

		coreControl.RemoveShell(this);

		SoundSetPlayer.PlayRandomFromSet(shellDie,core.sound.sourceGeneral);
		Physics.IgnoreCollision(gameObject.GetComponent<Collider>(),core.gameObject.GetComponent<Collider>(),false);
		rigidBody.useGravity = true;
		rigidBody.isKinematic = false;
		StopAllCoroutines();
		StartCoroutine(LerpBack(Color.red,1,true));
	}

	IEnumerator LerpBack(Color newColor,float duration, bool end)
	{
		// Transition from red to oldColor in 'duration' seconds.
		float elapsed = 0;
		while(elapsed < duration)
		{
			GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(newColor,Color.black,elapsed/duration));
			GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(newColor,oldColor,elapsed/duration);
			elapsed = elapsed + Time.fixedDeltaTime;
			yield return new WaitForEndOfFrame();
		}
		if(end)
			StartCoroutine(MarkForDelete());
	}

}
