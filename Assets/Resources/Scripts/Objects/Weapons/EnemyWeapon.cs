﻿using UnityEngine;
using System.Collections;

// Customizable enemy weapon.

public class EnemyWeapon : MonoBehaviour 
{
	public AudioClip shootSound;	// Sound to play when firing.
	public float roundsPerSecond;	// How many shots can be taken per second.
	public int roundDelayVariance;	// If not 0, adds some percentage to the firing delay so sound effects dont pile on.
	public int damage;				// Damage to deal.

	public GameObject projectile;		// Which projectile to fire.
	public Transform projectileOrigin;	// Where to fire the projectile from

	float fireDelay;				// How long we wait during shots.
	bool firing;					// Whether or not we're currently firing.

	Enemy.Control control;
	Enemy.Sound sound;

	Color oldColor;
	Color oldEmmissionColor;

	void Start () 
	{
		// Assigning this script to a player is unacceptable.
		if(gameObject.GetComponent<Player>() != null)
			Destroy (this);

		control = gameObject.GetComponent<Enemy>().control;
		sound = gameObject.GetComponent<Enemy>().sound;
		fireDelay = 1/roundsPerSecond;

		// Effects Stuff
		this.gameObject.GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");

		Color tmp = GetComponentInChildren<MeshRenderer>().material.color;
		oldColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);

		tmp = GetComponentInChildren<MeshRenderer>().material.GetColor("_EmissionColor");
		oldEmmissionColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);
	}

	void Update () // If the enemy dies destroy the weapon, so corpses don't shoot.
	{
		if(gameObject.GetComponent<Enemy>().isAlive == false)
			Destroy (this);
		if(control.tracker.tracking && !firing)
		{
			StartCoroutine(Fire ());
		}
	}

	public void SetRateOfFire(float _roundsPerSecond) // Calculate the fireDelay based on rate of fire.
	{
		fireDelay = 1/_roundsPerSecond;
	}

	IEnumerator Fire()
	{
		firing = true;
		while (true)
		{
			if(!control.tracker.tracking) break;
			Shoot();
			yield return new WaitForSeconds(fireDelay);
			yield return new WaitForSeconds(1f-(1f/Random.Range(1,roundDelayVariance)));
		}
		firing = false;
	}

	void Shoot()	// Called once per loop in the Fire Coroutine.
	{
			StartCoroutine(LerpBack(.5f));
			SoundSetPlayer.PlaySound(sound.sourceGeneral, shootSound);
			
			Vector3 direction = control.raycaster.CastRay(projectileOrigin.position, control.tracker.lastDirection, 0);
			GameObject tmp = Instantiate (projectile,projectileOrigin.position,new Quaternion()) as GameObject;
			tmp.GetComponent<Projectile>().Init(direction, damage);
	}

	IEnumerator LerpBack(float duration)	// Fancy Effects.
	{
		// Transition from red to oldColor in 'duration' seconds.
		float elapsed = 0;
		while(elapsed < duration)
		{
			GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(Color.red,oldEmmissionColor,elapsed/duration));
			GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(Color.red,oldColor,elapsed/duration);
			elapsed = elapsed + Time.fixedDeltaTime;
			yield return new WaitForEndOfFrame();
		}
	}
}
