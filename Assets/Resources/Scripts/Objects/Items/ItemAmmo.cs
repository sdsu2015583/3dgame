﻿using UnityEngine;
using System.Collections;

// Ammo variant of Item, when picked up a new entry is added to ammoStorage in PlayerAmmoTracker,
// and the amount of ammo specified by this item is added to that entry.

public class ItemAmmo : Item 
{
	[HideInInspector] public PlayerAmmoTracker.AmmoType ammoTypeInstance;
	public string ammoType;

	void Start()
	{
		if(GameObject.Find("Items") != null)
			gameObject.transform.parent = GameObject.Find("Items").transform;
		ammoTypeInstance = new PlayerAmmoTracker.AmmoType(ammoType);
	}

	public override void OnPickup()
	{
		player.gameObject.GetComponent<PlayerAmmoTracker>().AddAmmo(this);
	}
}
