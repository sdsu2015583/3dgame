﻿using UnityEngine;
using System.Collections;

// Objective variant of Item.

public class ItemObjective : Item 
{
	public override void OnPickup()
	{
		player.hasObjective = true;
	}
}
