﻿using UnityEngine;
using System.Collections;

// Armor variant of Item.

public class ItemArmor : Item
{
	public override void OnPickup()
	{
		player.AddArmor(value);
	}
}

