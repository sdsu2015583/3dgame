SDSU SPRING 2015 CS583 - 3D GAME PROGRAMMING
Group 6 - "Team TBD"

Amanda Minieri
Eduardo Devera
Erick Martinez
Victor Chan

Game Title: Spheroid

Note:
    
    The majority of the work done was laying the framework for the game. 
    The levels created for this demonstration were made the night before 
    for the purpose of showcasing the work as best as possible in the time alloted.
    
    Framework:
    
        Dictionary Ammunition Tracking.
        Base classes for Actors/Items/Weapons that can be easily modified/extended for unique behavior.
        Save states, loading and saving, serialization of relevant objects.
        Enemy player tracking, and movement.
        
    Scrips are located in /Assets/Resources/Scripts/

//-- HOW TO PLAY

    Controls:
    
        MOUSE   AIM
    
        W       MOVE FOWARD
        A       MOVE LEFT
        S       MOVE BACK
        D       MOVE RIGHT
        
        G       DROP PICKED UP WEAPON
        E       OPEN DOOR
        
        LMB     FIRE WEAPON
    
    Objective:
    
        Search the map for the glowing green cylinder, killing enemies on the way with weapons found in the level.
        Once you've found the objective item you may pass through the orange door.
                
        Stepping on the green warp panel will take you to the next area. This panel will be behind the objective door.
        
        Pink/Purple doors may be opened at will.

    Items:
    
        ARMOR   BLUE ROTATING CUBE      - Reduces the amount of damage taken by 85%, the amout of armor depleted per hit is equal to the amount of damage dealt.
        HEALTH  RED ROTATING CAPSULE    - If you run out of health you die, and the game ends. You can then either load from your last save, restart the level, or return to the main menu to quit.
        AMMO    ORANGE ROTATING ITEM    - You must have the ammo that matches the gun you're currently weilding in order to fire.

    Enemies:
        
        SPHEROID "GRUNT"    - Pursue, and attempt to kill the player. They do little damage and have little health.
        SPHEROID "ELITE"    - Like the grunt, but the Elite will defend itself with its outer shields. When the current shield is destroyed a new one is rotated into place.
                                Elites do far more damage than Grunts, and have much more armor.
        
    Saving/Loading:
    
        You may save and load at any time.
        Using a warp panel will automatically save your progress.
        Restarting a level will revert your stats to what they were when you entered the level.
        
        Save data may be cleared in the options section of the main menu (Or through the in-game menu, though that button is only for testing purposes).

//-- BUGS / ISSUES
    
Known Bugs / Outstanding Issues:

    Grunts and Elite core corpses not properly cleaned up by garbage collector.
    Audio sliders in menu start out zeroed. Music might start out muted. Adjusting these sliders and applying solves this.
    
Band-Aid Bug Fixes:     
         
    EnemyRaycaster.cs   : If the raycast from IsPlayerVisible encounters a weapon
    Weapon.cs             instead of a player, it checks if the weapon is equipped.
                          If the weapon is equipped it is assumed that a player is
                          is holding it. So it returns true.
    
    Weapon.cs           : Collisions are re-enabled before the collisions are disabled.
                          Otherwise we get a warning.
                          
    SaveTag.cs          : isArmed is forced to false in the Create method for PlayerTag
                          This is so that weapons can be picked up when the state is loaded.
                          
    ItemAmmo.cs         : Re-added grouping code to Start function, because the local Start
                          function overrode behavior from the base Item class.

    There may be a few others.
    
//-- REFERENCES / ASSET SOURCES
        
Sound Sources:

    Music Loop      : System Shock 2 OST

    EnemyEnergyShot : Fallout 3 - Plasma Pistol Fire
    EliteEnergyShot : Fallout 3 - Guass Rifle Fire
    NoAmmo          : Firearms Source - MAC10 Slide
    PickupAmmo      : Firearms Source - G3SG1 ClipIn
    Set_Shotgun     : Firearms Source - Sawn Off Shotgun

    EliteDeath      : Half-Life 2 Standard GCF Mix
                        Electric Machine Loop
                        Combine Terminal Idle
                        Airboat Motor Spindown
                        Catapult Throw
    
    Set_MetalImpactSpecial  : Half-Life 2 Standard GCF Mix
                                Metal Box Impact
                                Metal Cansiter Impact
                                Metal Groan
                                
    Set_MetalImpact : Half-Life 2 Standard GCF                           
    Set_ShellDrop   : Half-Life 2 Standard GCF
    White Flash     : Half-Life 2 Standard GCF
    Klaxon 1        : Half-Life 2 Standard GCF
    Warning Bell 1  : Half-Life 2 Standard GCF
    
    All unlisted sounds produced with Audacity.

Texture Sources:
    All unlisted textures produced with Adobe Photoshop CS6

Model Sources:
    All unlisted models produced with Autodesk 3dsnax 2015 and Hammer World Editor
    
    
First Person Control Script:
    The one that came with unity (modified slightly).
    
Code References:
    http://answers.unity3d.com/questions/28050/how-to-group-variables-in-the-inspector.html
    https://msdn.microsoft.com/en-us/library/system.activator.createinstance%28v=vs.110%29.aspx
    http://stackoverflow.com/questions/840261/passing-arguments-to-c-sharp-generic-new-of-templated-type
    https://msdn.microsoft.com/en-us/library/512aeb7t.aspx

SerializerBinary.cs based on script created in video by "HardlyBriefDan"
    https://www.youtube.com/watch?v=ahLllS_jR5s
    
CameraAspectRatio.cs 
    http://gamedesigntheory.blogspot.ie/2010/09/controlling-aspect-ratio-in-unity.html
    
General pointers on generics for SaveController.cs
    Matt Hoffman (@LordNed)